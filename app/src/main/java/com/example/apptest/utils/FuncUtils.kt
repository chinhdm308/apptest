package com.example.apptest.utils

import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.provider.OpenableColumns
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

class FuncUtils {
}

fun getImageSize(context: Context, uri: Uri): Long {
    val cursor: Cursor? = context.contentResolver.query(uri, null, null, null, null)
    if (cursor != null) {
        val sizeIndex: Int = cursor.getColumnIndex(OpenableColumns.SIZE)
        cursor.moveToFirst()
        val imageSize: Long = cursor.getLong(sizeIndex)
        cursor.close()
        return imageSize // returns size in bytes
    }
    return 0
}

fun getBitmapFromUri(context: Context, fileUri: Uri): Bitmap {
    try {
        val matrix = Matrix()
        context.contentResolver.openInputStream(fileUri)?.use {
            val exifInterface = ExifInterface(it)
            val orientation: Int = exifInterface.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )
            val rotationInDegrees = exifToDegrees(orientation)
            matrix.preRotate(rotationInDegrees.toFloat())
        }
        val bitmap = context.contentResolver.openFileDescriptor(fileUri, "r")?.use {
            val fileDescriptor = it.fileDescriptor
            val image: Bitmap? = BitmapFactory.decodeFileDescriptor(fileDescriptor)
            image
        }
        return Bitmap.createBitmap(bitmap!!, 0, 0, bitmap.width, bitmap.height, matrix, true)
    } catch (e: Exception) {
        return Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
    }
}

fun exifToDegrees(exifOrientation: Int): Int = when (exifOrientation) {
    ExifInterface.ORIENTATION_ROTATE_90 -> 90
    ExifInterface.ORIENTATION_ROTATE_180 -> 180
    ExifInterface.ORIENTATION_ROTATE_270 -> 270
    else -> 0
}

fun createTemporalFileFrom(
    context: Context,
    inputStream: InputStream?
): File? {
    var targetFile: File? = null
    if (inputStream != null) {
        var read: Int
        val buffer = ByteArray(9 * 1024)
        targetFile = File(context.cacheDir, "${System.currentTimeMillis()}.jpg")
        val outputStream: OutputStream = FileOutputStream(targetFile)
        while (inputStream.read(buffer).also { read = it } != -1) {
            outputStream.write(buffer, 0, read)
        }
        outputStream.flush()
        try {
            outputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    return targetFile
}