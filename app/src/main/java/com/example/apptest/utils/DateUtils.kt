package com.example.apptest.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat

class DateUtils {
    companion object {
        const val enDate = "yyyy-MM-dd"
        const val fullDate = "yyyy-MM-dd'T'HH:mm:ss"
    }
}

@SuppressLint("SimpleDateFormat")
fun String.convertTo(from: String, to: String): String {
    return try {
        val formatterTo = SimpleDateFormat(to)
        val formatterFrom = SimpleDateFormat(from)
        return formatterTo.format(formatterFrom.parse(this)!!)
    } catch (e: Exception) {
        ""
    }
}