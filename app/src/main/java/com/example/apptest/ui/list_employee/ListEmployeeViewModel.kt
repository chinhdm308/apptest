package com.example.apptest.ui.list_employee

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.apptest.data.model.EmployeeResponse
import com.example.apptest.data.remote.ApiServices
import com.example.apptest.data.source.EmployeesDataSource
import com.example.apptest.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListEmployeeViewModel @Inject constructor(
    private val apiServices: ApiServices,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val LAST_SEARCH_QUERY = "LAST_SEARCH_QUERY"

    private var initialQuery: String
        get() = savedStateHandle.get<String>(LAST_SEARCH_QUERY) ?: ""
        set(value) = savedStateHandle.set(LAST_SEARCH_QUERY, value)

    private val searches = MutableSharedFlow<String>()

    @OptIn(ExperimentalCoroutinesApi::class)
    val pagingDataFlow = searches
        .flatMapLatest { getListEmployee(fullName = it) }
        .cachedIn(viewModelScope)

    fun init() {
        viewModelScope.launch {
            searches.emit(initialQuery)
        }
    }

    fun search(query: String) {
        viewModelScope.launch {
            initialQuery = query
            searches.emit(query)
        }
    }

    fun deleteEmployee(id: Int) {
        viewModelScope.launch {
            flow {
                val response = apiServices.deleteEmployee(id)
                emit(response)
            }.flowOn(Dispatchers.IO)
                .onStart { showLoading.emit(true) }
                .onCompletion { showLoading.emit(false) }
                .catch {
                    errorMessage.emit("Đã xảy ra lỗi")
                }
                .collect {
                    search(initialQuery)
                }
        }
    }

    private fun getListEmployee(fullName: String): Flow<PagingData<EmployeeResponse>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10
            ),
            pagingSourceFactory = { EmployeesDataSource(apiServices, fullName) }
        ).flow.cachedIn(viewModelScope)
    }
}