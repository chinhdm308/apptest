package com.example.apptest.ui.base

import android.util.Log
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import javax.inject.Inject

@HiltViewModel
open class BaseViewModel @Inject constructor() : ViewModel() {
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("COROUTINE EXCEPTION", "${throwable.message}")
    }

    val errorMessage = MutableSharedFlow<String>()

    val alertMessage = MutableSharedFlow<String>()

    val showLoading = MutableSharedFlow<Boolean>()
}