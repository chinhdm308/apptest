package com.example.apptest.ui.update_employee

import android.app.DatePickerDialog
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.apptest.databinding.FragmentUpdateEmployeeBinding
import com.example.apptest.ui.base.BaseFragment
import com.example.apptest.ui.base.BaseViewModel
import com.example.apptest.utils.DateUtils
import com.example.apptest.utils.convertTo
import dagger.hilt.android.AndroidEntryPoint
import java.util.Calendar

@AndroidEntryPoint
class UpdateEmployeeFragment :
    BaseFragment<FragmentUpdateEmployeeBinding>(FragmentUpdateEmployeeBinding::inflate) {

    private val viewModel by viewModels<UpdateEmployeeViewModel>()

    val listDepartment = listOf(
        "Ban giám đốc", "Phòng Kế toán", "Phòng Kinh doanh", "Phòng Thiết kế"
    )

    private val adapterItem by lazy {
        ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, listDepartment)
    }

    override fun getVM(): BaseViewModel = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        binding.textDepartment.setAdapter(adapterItem)

        viewModel.detail?.let {
            binding.textName.setText(it.fullName)
            binding.textBirthday.setText(
                it.birthday?.convertTo(
                    DateUtils.fullDate,
                    DateUtils.enDate
                )
            )
            if (it.gender == true) {
                binding.radioButtonMale.isChecked = true
            } else {
                binding.radioButtonFemale.isChecked = true
            }
            binding.textEmail.setText(it.email)
            binding.textPhoneNumber.setText(it.phoneNumber)
            binding.textSalary.setText(it.salary.toString())
            binding.textBiography.setText(it.biography)

            it.department?.let { department ->
                binding.textDepartment.setText(listDepartment[department - 1], false)
            }
        }

        binding.textDepartment.setOnItemClickListener { parent, view, position, id ->
            val item = parent.getItemAtPosition(position).toString()
            viewModel.detail?.department = position + 1
        }

        binding.textBirthday.showSoftInputOnFocus = false
        binding.textBirthday.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog = DatePickerDialog(
                requireContext(),
                { view, year, monthOfYear, dayOfMonth ->
                    val dat =
                        (year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString())
                    binding.textBirthday.setText(dat)
                    viewModel.detail?.birthday = dat
                },
                year,
                month,
                day
            )
            datePickerDialog.show()
        }

        binding.buttonBack.setOnClickListener {
            navController.navigateUp()
        }

        binding.buttonSave.setOnClickListener {
            viewModel.detail?.let {
                it.fullName = binding.textName.text.toString().trim()
                it.email = binding.textEmail.text.toString().trim()
                it.phoneNumber = binding.textPhoneNumber.text.toString().trim()
                it.salary = binding.textSalary.text.toString().trim().toInt()
                it.biography = binding.textBiography.text.toString().trim()
                it.gender = binding.radioButtonMale.isChecked
            }
            viewModel.updateEmployee()
        }
    }

}