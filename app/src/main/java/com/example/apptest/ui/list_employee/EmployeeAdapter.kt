package com.example.apptest.ui.list_employee

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.apptest.R
import com.example.apptest.data.model.EmployeeResponse
import com.example.apptest.databinding.ItemEmployeeBinding

class EmployeeAdapter(
    private val context: Context,
    val onDetail: (EmployeeResponse) -> Unit,
    val onDelete: (Int) -> Unit,
    val onEdit: (EmployeeResponse) -> Unit
) : PagingDataAdapter<EmployeeResponse, EmployeeAdapter.EmployeeViewHolder>(EmployeeDiffUtil()) {

    private val circularProgressDrawable by lazy {
        CircularProgressDrawable(context).apply {
            strokeWidth = 5f
            centerRadius = 30f
            start()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val binding = ItemEmployeeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EmployeeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item!!)
    }

    inner class EmployeeViewHolder(private val binding: ItemEmployeeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: EmployeeResponse) {
            binding.textName.text = binding.root.context.getString(R.string.name, item.fullName)
            binding.textEmail.text = binding.root.context.getString(R.string.email, item.email)
            binding.textPhoneNumber.text = binding.root.context.getString(R.string.phone_number, item.phoneNumber)
            binding.textDepartment.text = binding.root.context.getString(R.string.department, item.department.toString())
            Glide.with(binding.root.context)
                .load(item.image)
                .placeholder(circularProgressDrawable)
                .error(R.drawable.default_avatar)
                .circleCrop()
                .into(binding.imgAvatar)

            binding.textName.setOnClickListener {
                onDetail.invoke(item)
            }

            binding.buttonUpdate.setOnClickListener {
                onEdit.invoke(item)
            }

            binding.buttonDelete.setOnClickListener {
                if (item.id == null) return@setOnClickListener
                onDelete.invoke(item.id!!)
            }
        }
    }
}

class EmployeeDiffUtil : DiffUtil.ItemCallback<EmployeeResponse>() {
    override fun areItemsTheSame(oldItem: EmployeeResponse, newItem: EmployeeResponse): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: EmployeeResponse, newItem: EmployeeResponse): Boolean {
        return oldItem.id == newItem.id
    }

}