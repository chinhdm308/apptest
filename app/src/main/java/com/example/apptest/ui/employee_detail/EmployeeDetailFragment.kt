package com.example.apptest.ui.employee_detail

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.webkit.MimeTypeMap
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.graphics.applyCanvas
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.apptest.R
import com.example.apptest.databinding.FragmentEmployeeDetailBinding
import com.example.apptest.ui.base.BaseFragment
import com.example.apptest.ui.base.BaseViewModel
import com.example.apptest.utils.DateUtils
import com.example.apptest.utils.convertTo
import com.example.apptest.utils.createTemporalFileFrom
import com.example.apptest.utils.exifToDegrees
import com.example.apptest.utils.getBitmapFromUri
import com.example.apptest.utils.getImageSize
import dagger.hilt.android.AndroidEntryPoint
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

@AndroidEntryPoint
class EmployeeDetailFragment :
    BaseFragment<FragmentEmployeeDetailBinding>(FragmentEmployeeDetailBinding::inflate) {

    private val viewModel by viewModels<EmployeeDetailViewModel>()

    private val circularProgressDrawable by lazy {
        CircularProgressDrawable(requireContext()).apply {
            strokeWidth = 5f
            centerRadius = 30f
            start()
        }
    }

    private var selectImageFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let {
//                previewImage.setImageURI(uri)
                resolve(it)
            }
        }

    private val requestPermissionCollection = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions(),
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.READ_EXTERNAL_STORAGE, false) ||
                    permissions.getOrDefault(Manifest.permission.READ_MEDIA_IMAGES, false) -> {
                selectImageFromGallery()
            }

            else -> {
                val showRationale =
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                        shouldShowRequestPermissionRationale(
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                    } else {
                        shouldShowRequestPermissionRationale(Manifest.permission.READ_MEDIA_IMAGES)
                    }
                if (!showRationale) {
                    viewModel.showMessage("Bạn cần cấp quyền truy cập cho ứng dụng")
                }
            }
        }
    }

    override fun getVM(): BaseViewModel = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        binding.toolbar.textTitle.text = "Chi tiết"

        viewModel.detail?.let {
            binding.textName.text = getString(R.string.name, it.fullName)
            binding.textBirthday.text = getString(
                R.string.birthday,
                "${it.birthday?.convertTo(DateUtils.fullDate, DateUtils.enDate)}"
            )
            binding.textGender.text =
                getString(R.string.gender, if (it.gender == true) "Nam" else "Nữ")
            binding.textEmail.text = getString(R.string.email, it.email)
            binding.textPhoneNumber.text = getString(R.string.phone_number, it.phoneNumber)
            binding.textSalary.text = getString(R.string.salary, it.salary.toString())
            binding.textDepartment.text = getString(R.string.department, it.department.toString())
            binding.textBiography.text = getString(R.string.biography, it.biography)

            Glide.with(requireContext())
                .load(it.image)
                .placeholder(circularProgressDrawable)
                .error(R.drawable.default_avatar)
                .circleCrop()
                .into(binding.imgAvatar)
        }
    }

    override fun setOnClick() {
        super.setOnClick()

        binding.imgAvatar.setOnClickListener {
            binding.buttonUpdateAvatar.isVisible = true
        }

        binding.buttonUpdateAvatar.setOnClickListener {
            requestPermissionCollection.launch(
                arrayOf(
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    } else {
                        Manifest.permission.READ_MEDIA_IMAGES
                    }
                )
            )
        }

        binding.toolbar.buttonBack.setOnClickListener {
            navController.navigateUp()
        }
    }

    private fun selectImageFromGallery() = selectImageFromGalleryResult.launch("image/*")

    private fun resolve(image: Uri) {
        val mimeTypeMap = MimeTypeMap.getSingleton()
        val byte = getImageSize(requireContext(), image)
        val bitmap = getBitmapFromUri(requireContext(), image)
        if (byte > 1 * 1000000 ||
            bitmap.width <= 256 ||
            bitmap.height <= 256
        ) {
            viewModel.showMessage(message = "Ảnh lỗi")
        } else {
            resizeImage(bitmap)?.let { viewModel.uploadAvatar(it) }
        }
    }

    private fun resizeImage(bitmap: Bitmap): File? {
        val resizeBitmap = if (bitmap.width > bitmap.height) {
            var newWidth = 256F
            var newHeight = 256 * (bitmap.height.toFloat() / bitmap.width.toFloat())
            if (newHeight > 256) {
                newWidth = 256 * (newWidth / newHeight)
                newHeight = 256F
            }
            Bitmap.createScaledBitmap(bitmap, newWidth.toInt(), newHeight.toInt(), true)
        } else if (bitmap.width < bitmap.height) {
            val newHeight = 256F
            val newWidth = 256 * (bitmap.width.toFloat() / bitmap.height.toFloat())
            Bitmap.createScaledBitmap(bitmap, newWidth.toInt(), newHeight.toInt(), true)
        } else {
            Bitmap.createScaledBitmap(bitmap, 256, 256, true)
        }
        val canvasWidth = 256
        val canvasHeight = 256
        val bitmapNew = Bitmap.createBitmap(canvasWidth, canvasHeight, Bitmap.Config.ARGB_8888)
        val centreX = (canvasWidth - resizeBitmap.width) / 2
        val centreY = (canvasHeight - resizeBitmap.height) / 2

        val paint = Paint()
        paint.isAntiAlias = true
        paint.isFilterBitmap = true
        paint.isDither = true
        bitmapNew.applyCanvas {
            val canvas = Canvas(bitmapNew)
            canvas.drawColor(Color.TRANSPARENT)
            canvas.drawBitmap(resizeBitmap, centreX.toFloat(), centreY.toFloat(), paint)
        }
        val bos = ByteArrayOutputStream()
        bitmapNew.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        val byteArray: ByteArray = bos.toByteArray()
        bitmap.recycle()
        resizeBitmap.recycle()
        bitmapNew.recycle()
        return createTemporalFileFrom(requireContext(), ByteArrayInputStream(byteArray))
    }
}