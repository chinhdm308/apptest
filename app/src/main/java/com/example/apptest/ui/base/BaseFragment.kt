package com.example.apptest.ui.base

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.android.ai_music.ui.base.ViewInterface
import com.example.apptest.ui.main.MainActivity
import com.example.apptest.utils.collectLifecycleFlow

abstract class BaseFragment<VB : ViewBinding>(private val inflate: FragmentBindingInflater<VB>) :
    Fragment(), ViewInterface {

    private var _binding: VB? = null
    protected val binding: VB
        get() = _binding
            ?: throw IllegalStateException("Cannot access view after view destroyed or before view creation")

    protected val navController get() = findNavController()

    private lateinit var viewModel: BaseViewModel
    abstract fun getVM(): BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getVM()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(savedInstanceState)

        setOnClick()

        bindingStateView()

        bindingAction()

        with(viewModel) {
            collectLifecycleFlow(showLoading) {
                (activity as? MainActivity)?.showHideLoading(it)
            }

            collectLifecycleFlow(errorMessage) {
                showErrorDialog(it)
            }

            collectLifecycleFlow(alertMessage) {
                showAlertDialog(it)
            }
        }
    }

    private fun showAlertDialog(mess: String) {
        AlertDialog.Builder(requireContext())
            .setTitle("Thông báo")
            .setMessage(mess)
            .setPositiveButton(android.R.string.ok) { _, _ ->

            }
            .show()
    }

    private fun showErrorDialog(errorMes: String) {
        AlertDialog.Builder(requireContext())
            .setTitle("Lỗi")
            .setMessage(errorMes)
            .setPositiveButton(android.R.string.ok) { _, _ ->

            }
            .show()
    }

    fun showHideLoading(isShow: Boolean) {
        (activity as? MainActivity)?.showHideLoading(isShow)
    }

    override fun setOnClick() {}

    override fun initView(savedInstanceState: Bundle?) {}

    override fun bindingStateView() {}

    override fun bindingAction() {}

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}