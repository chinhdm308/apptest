package com.example.apptest.ui.add_employee

import androidx.lifecycle.viewModelScope
import com.example.apptest.data.remote.ApiServices
import com.example.apptest.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

@HiltViewModel
class AddEmployeeViewModel @Inject constructor(
    private val apiServices: ApiServices
) : BaseViewModel() {

    private val _uiState = MutableSharedFlow<UiState>()
    val uiState get() = _uiState.asSharedFlow()

    var fileImage: File? = null
    var department: Int = -1

    fun showMessage(message: String) {
        viewModelScope.launch {
            errorMessage.emit(message)
        }
    }

    fun createEmployee(
        fullName: String?,
        birthday: String?,
        gender: Boolean,
        email: String?,
        phoneNumber: String?,
        salary: String?,
        biography: String?,
    ) {
        viewModelScope.launch {
            if (fullName.isNullOrEmpty() || birthday.isNullOrEmpty() ||  email.isNullOrEmpty() || salary.isNullOrEmpty() || department == -1
            ) {
                errorMessage.emit("Vui lòng nhập đủ thông tin: Họ tên, ngày sinh, giới tính, địa chỉ email, thu nhập, phòng ban")
                return@launch
            }

            if (fileImage == null) {
                errorMessage.emit("Vui lòng chọn ảnh đại diện")
                return@launch
            }

            val fileReqBody = fileImage!!.asRequestBody("multipart/form-data".toMediaTypeOrNull())
            val image = MultipartBody.Part.createFormData("image", fileImage!!.name, fileReqBody)

            flow {
                val response = apiServices.createEmployee(
                    fullName = fullName.toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    birthday = birthday.toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    gender = gender.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    email = email.toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    phoneNumber = phoneNumber?.toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    salary = salary.toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    department = department.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    biography = biography?.toRequestBody("multipart/form-data".toMediaTypeOrNull()),
                    image = image
                )
                emit(response)
            }.flowOn(Dispatchers.IO)
                .onStart { showLoading.emit(true) }
                .onCompletion { showLoading.emit(false) }
                .catch {
                    errorMessage.emit("Đã xảy ra lỗi")
                }
                .collect {
                    alertMessage.emit("Cập nhật thành công")
                    _uiState.emit(UiState.CreateSuccess)
                }

        }
    }

    sealed class UiState {
        object CreateSuccess : UiState()
    }
}