package com.example.apptest.ui.add_employee

import android.Manifest
import android.R
import android.app.DatePickerDialog
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.webkit.MimeTypeMap
import android.widget.ArrayAdapter
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.graphics.applyCanvas
import androidx.fragment.app.viewModels
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.example.apptest.databinding.FragmentAddEmployeeBinding
import com.example.apptest.ui.base.BaseFragment
import com.example.apptest.ui.base.BaseViewModel
import com.example.apptest.utils.collectLifecycleFlow
import com.example.apptest.utils.createTemporalFileFrom
import com.example.apptest.utils.getBitmapFromUri
import com.example.apptest.utils.getImageSize
import dagger.hilt.android.AndroidEntryPoint
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.Calendar

@AndroidEntryPoint
class AddEmployeeFragment :
    BaseFragment<FragmentAddEmployeeBinding>(FragmentAddEmployeeBinding::inflate) {

    private val viewModel by viewModels<AddEmployeeViewModel>()

    val listDepartment = listOf(
        "Ban giám đốc", "Phòng Kế toán", "Phòng Kinh doanh", "Phòng Thiết kế"
    )

    private val adapterItem by lazy {
        ArrayAdapter(requireContext(), R.layout.simple_list_item_1, listDepartment)
    }

    private val circularProgressDrawable by lazy {
        CircularProgressDrawable(requireContext()).apply {
            strokeWidth = 5f
            centerRadius = 30f
            start()
        }
    }

    private var selectImageFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let {
                resolve(it)
            }
        }

    private val requestPermissionCollection = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions(),
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.READ_EXTERNAL_STORAGE, false) ||
                    permissions.getOrDefault(Manifest.permission.READ_MEDIA_IMAGES, false) -> {
                selectImageFromGallery()
            }

            else -> {
                val showRationale =
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                        shouldShowRequestPermissionRationale(
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                    } else {
                        shouldShowRequestPermissionRationale(Manifest.permission.READ_MEDIA_IMAGES)
                    }
                if (!showRationale) {
                    viewModel.showMessage("Bạn cần cấp quyền truy cập cho ứng dụng")
                }
            }
        }
    }

    override fun getVM(): BaseViewModel {
        return viewModel
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        binding.toolbar.textTitle.text = "Thêm mới"

        binding.textDepartment.setAdapter(adapterItem)

        binding.textBirthday.showSoftInputOnFocus = false
    }

    override fun setOnClick() {
        super.setOnClick()

        binding.textDepartment.setOnItemClickListener { parent, view, position, id ->
            viewModel.department = position + 1
        }

        binding.textBirthday.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog = DatePickerDialog(
                requireContext(),
                { view, year, monthOfYear, dayOfMonth ->
                    val dat =
                        (year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString())
                    binding.textBirthday.setText(dat)
                },
                year,
                month,
                day
            )
            datePickerDialog.show()
        }

        binding.toolbar.buttonBack.setOnClickListener {
            navController.navigateUp()
        }

        binding.imgAvatar.setOnClickListener {
            requestPermissionCollection.launch(
                arrayOf(
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    } else {
                        Manifest.permission.READ_MEDIA_IMAGES
                    }
                )
            )
        }

        binding.buttonSave.setOnClickListener {
            viewModel.createEmployee(
                fullName = binding.textName.text.toString().trim(),
                birthday = binding.textBirthday.text.toString().trim(),
                gender = binding.radioButtonMale.isChecked,
                email = binding.textEmail.text.toString().trim(),
                phoneNumber = binding.textPhoneNumber.text.toString().trim(),
                salary = binding.textSalary.text.toString().trim(),
                biography = binding.textBiography.text.toString().trim(),
            )
        }
    }

    override fun bindingStateView() {
        super.bindingStateView()

        collectLifecycleFlow(viewModel.uiState) { state ->
            if (state is AddEmployeeViewModel.UiState.CreateSuccess) {
                navController.navigateUp()
            }
        }
    }

    private fun selectImageFromGallery() = selectImageFromGalleryResult.launch("image/*")

    private fun resolve(image: Uri) {
        val byte = getImageSize(requireContext(), image)
        val bitmap = getBitmapFromUri(requireContext(), image)
        if (byte > 1 * 1000000 ||
            bitmap.width <= 256 ||
            bitmap.height <= 256
        ) {
            viewModel.showMessage(message = "Ảnh lỗi")
        } else {
            resizeImage(bitmap)?.let {
                viewModel.fileImage = it

                Glide.with(requireContext())
                    .load(it)
                    .placeholder(circularProgressDrawable)
                    .error(com.example.apptest.R.drawable.default_avatar)
                    .circleCrop()
                    .into(binding.imgAvatar)
            }
        }
    }

    private fun resizeImage(bitmap: Bitmap): File? {
        val resizeBitmap = if (bitmap.width > bitmap.height) {
            var newWidth = 256F
            var newHeight = 256 * (bitmap.height.toFloat() / bitmap.width.toFloat())
            if (newHeight > 256) {
                newWidth = 256 * (newWidth / newHeight)
                newHeight = 256F
            }
            Bitmap.createScaledBitmap(bitmap, newWidth.toInt(), newHeight.toInt(), true)
        } else if (bitmap.width < bitmap.height) {
            val newHeight = 256F
            val newWidth = 256 * (bitmap.width.toFloat() / bitmap.height.toFloat())
            Bitmap.createScaledBitmap(bitmap, newWidth.toInt(), newHeight.toInt(), true)
        } else {
            Bitmap.createScaledBitmap(bitmap, 256, 256, true)
        }
        val canvasWidth = 256
        val canvasHeight = 256
        val bitmapNew = Bitmap.createBitmap(canvasWidth, canvasHeight, Bitmap.Config.ARGB_8888)
        val centreX = (canvasWidth - resizeBitmap.width) / 2
        val centreY = (canvasHeight - resizeBitmap.height) / 2

        val paint = Paint()
        paint.isAntiAlias = true
        paint.isFilterBitmap = true
        paint.isDither = true
        bitmapNew.applyCanvas {
            val canvas = Canvas(bitmapNew)
            canvas.drawColor(Color.TRANSPARENT)
            canvas.drawBitmap(resizeBitmap, centreX.toFloat(), centreY.toFloat(), paint)
        }
        val bos = ByteArrayOutputStream()
        bitmapNew.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        val byteArray: ByteArray = bos.toByteArray()
        bitmap.recycle()
        resizeBitmap.recycle()
        bitmapNew.recycle()
        return createTemporalFileFrom(requireContext(), ByteArrayInputStream(byteArray))
    }
}