package com.example.apptest.ui.update_employee

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.apptest.data.model.EmployeeResponse
import com.example.apptest.data.remote.ApiServices
import com.example.apptest.ui.base.BaseViewModel
import com.example.apptest.utils.BUNDLE_EMPLOYEE_DETAIL
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UpdateEmployeeViewModel @Inject constructor(
    private val apiServices: ApiServices,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {
    val detail: EmployeeResponse?
        get() = savedStateHandle.get<EmployeeResponse>(BUNDLE_EMPLOYEE_DETAIL)

    fun updateEmployee() {
        viewModelScope.launch {
            if (detail == null || detail?.id == null) return@launch

            if (detail?.fullName.isNullOrEmpty() || detail?.birthday.isNullOrEmpty() || detail?.gender?.toString()
                    .isNullOrEmpty() || detail?.email.isNullOrEmpty() || detail?.salary?.toString()
                    .isNullOrEmpty() || detail?.department?.toString().isNullOrEmpty()
            ) {
                errorMessage.emit("Vui lòng nhập đủ thông tin: Họ tên, ngày sinh, giới tính, địa chỉ email, thu nhập, phòng ban")
                return@launch
            }

            val body = mapOf(
                "id" to detail!!.id!!.toString(),
                "fullName" to detail!!.fullName!!,
                "birthday" to detail!!.birthday!!,
                "gender" to detail!!.gender!!.toString(),
                "email" to detail!!.email!!,
                "phoneNumber" to (detail!!.phoneNumber ?: ""),
                "salary" to detail!!.salary!!.toString(),
                "department" to detail!!.department!!.toString(),
                "biography" to (detail!!.biography ?: "")
            )

            flow {
                val response = apiServices.updateEmployee(body)
                emit(response)
            }.flowOn(Dispatchers.IO)
                .onStart { showLoading.emit(true) }
                .onCompletion { showLoading.emit(false) }
                .catch {
                    errorMessage.emit("Đã xảy ra lỗi")
                }
                .collect {
                    alertMessage.emit("Cập nhật thành công")
                }
        }
    }
}