package com.example.apptest.ui.employee_detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.apptest.data.model.EmployeeResponse
import com.example.apptest.data.remote.ApiServices
import com.example.apptest.ui.base.BaseViewModel
import com.example.apptest.utils.BUNDLE_EMPLOYEE_DETAIL
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

@HiltViewModel
class EmployeeDetailViewModel @Inject constructor(
    private val apiServices: ApiServices,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    val detail: EmployeeResponse?
        get() = savedStateHandle.get<EmployeeResponse>(BUNDLE_EMPLOYEE_DETAIL)

    fun showMessage(message: String) {
        viewModelScope.launch {
            errorMessage.emit(message)
        }
    }

    fun uploadAvatar(file: File) {
        viewModelScope.launch {
            if (detail?.id == null) return@launch

            val fileReqBody = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
            val image = MultipartBody.Part.createFormData("image", file.name, fileReqBody)

            val id = detail!!.id!!.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())

            flow {
                val response = apiServices.changeAvatar(
                    id = id,
                    image = image
                )
                emit(response)
            }.flowOn(Dispatchers.IO)
                .onStart { showLoading.emit(true) }
                .onCompletion { showLoading.emit(false) }
                .catch {
                    errorMessage.emit("Đã xảy ra lỗi")
                }
                .collect {
                    alertMessage.emit("Đã cập nhật ảnh đại diện")
                }
        }
    }

}