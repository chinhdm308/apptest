package com.example.apptest.ui.list_employee

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.apptest.R
import com.example.apptest.databinding.FragmentListEmployeeBinding
import com.example.apptest.ui.base.BaseFragment
import com.example.apptest.ui.base.BaseViewModel
import com.example.apptest.utils.BUNDLE_EMPLOYEE_DETAIL
import com.example.apptest.utils.collectLatestLifecycleFlow
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListEmployeeFragment : BaseFragment<FragmentListEmployeeBinding>(FragmentListEmployeeBinding::inflate) {

    private val viewModel: ListEmployeeViewModel by viewModels()

    private val employeeAdapter: EmployeeAdapter by lazy {
        EmployeeAdapter(
            requireContext(),
            onDelete = {
                viewModel.deleteEmployee(it)
            },
            onEdit = {
                navController.navigate(
                    R.id.action_listEmployeeFragment_to_updateEmployeeFragment,
                    bundleOf(
                        BUNDLE_EMPLOYEE_DETAIL to it
                    )
                )
            },
            onDetail = {
                navController.navigate(
                    R.id.action_listEmployeeFragment_to_employeeDetailFragment,
                    bundleOf(
                        BUNDLE_EMPLOYEE_DETAIL to it
                    )
                )
            }
        )
    }

    override fun getVM(): BaseViewModel = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        binding.toolbar.textTitle.text = "Danh sách nhân viên"
        binding.toolbar.buttonBack.isInvisible = true
        binding.toolbar.buttonAdd.isVisible = true

        binding.recycleEmployee.apply {
            val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            addItemDecoration(decoration)

            adapter = employeeAdapter
        }
    }

    override fun setOnClick() {
        super.setOnClick()

        binding.buttonSearch.setOnClickListener {
            viewModel.search(binding.edtSearch.text.toString().trim())
        }

        binding.toolbar.buttonAdd.setOnClickListener {
            navController.navigate(R.id.action_listEmployeeFragment_to_addEmployeeFragment)
        }
    }

    override fun bindingStateView() {
        super.bindingStateView()

        collectLatestLifecycleFlow(viewModel.pagingDataFlow) {
            employeeAdapter.submitData(it)
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.init()

    }
}