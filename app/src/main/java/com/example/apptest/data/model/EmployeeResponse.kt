package com.example.apptest.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class EmployeeResponse(
    @SerializedName("biography")
    var biography: String?,
    @SerializedName("birthday")
    var birthday: String?,
    @SerializedName("created_by")
    var createdBy: String?,
    @SerializedName("created_date")
    var createdDate: String?,
    @SerializedName("department")
    var department: Int?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("fullName")
    var fullName: String?,
    @SerializedName("gender")
    var gender: Boolean?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("image")
    var image: String?,
    @SerializedName("modified_by")
    var modifiedBy: String?,
    @SerializedName("modified_date")
    var modifiedDate: String?,
    @SerializedName("phoneNumber")
    var phoneNumber: String?,
    @SerializedName("salary")
    var salary: Int?,
) : Parcelable