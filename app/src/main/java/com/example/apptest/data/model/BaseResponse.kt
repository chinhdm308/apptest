package com.example.apptest.data.model

data class BaseResponse<T>(val content: T, val err: String?)