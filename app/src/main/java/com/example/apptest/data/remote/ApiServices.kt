package com.example.apptest.data.remote

import com.example.apptest.data.model.BasePagingResponse
import com.example.apptest.data.model.BaseResponse
import com.example.apptest.data.model.EmployeeResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServices {

    @Multipart
    @POST("/api/cms/Employee/Create")
    suspend fun createEmployee(
        @Part("fullName") fullName: RequestBody,
        @Part("birthday") birthday: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part("email") email: RequestBody,
        @Part("phoneNumber") phoneNumber: RequestBody?,
        @Part("salary") salary: RequestBody,
        @Part("department") department: RequestBody,
        @Part("biography") biography: RequestBody?,
        @Part image: MultipartBody.Part
    ): Any

    @PUT("/api/cms/Employee/Update")
    suspend fun updateEmployee(@Body body: Map<String, String>): Any

    @GET("/api/cms/Employee/GetList")
    suspend fun getListEmployees(
        @Query("pageIndex") pageIndex: Int = 1,
        @Query("pageSize") pageSize: Int = 10,
        @Query("fullName") fullName: String = ""
    ): BaseResponse<BasePagingResponse<List<EmployeeResponse>>>

    @GET("/api/cms/Employee/GetDetail/{id}")
    suspend fun getEmployeeDetail(@Path("id") id: Int): EmployeeResponse

    @DELETE("/api/cms/Employee/Delete/{id}")
    suspend fun deleteEmployee(@Path("id") id: Int): Any

    @Multipart
    @POST("/api/admin/Employee/ChangeAvatar")
    suspend fun changeAvatar(
        @Part("id") id: RequestBody,
        @Part image: MultipartBody.Part
    ): Any
}