package com.example.apptest.data.model

data class BasePagingResponse<T>(
    val items: T,
    val pageCount: Int?,
    val totalItemCount: Int?,
    val pageIndex: Int?,
    val pageSize: Int = 10,
    val hasPreviousPage: Boolean,
    val hasNextPage: Boolean
)
