package com.example.apptest.data.source

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.apptest.data.model.EmployeeResponse
import com.example.apptest.data.remote.ApiServices

class EmployeesDataSource(
    private val apiServices: ApiServices,
    private val fullName: String
) : PagingSource<Int, EmployeeResponse>() {
    override fun getRefreshKey(state: PagingState<Int, EmployeeResponse>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, EmployeeResponse> {
        return try {
            val nextPage: Int = params.key ?: FIRST_PAGE_INDEX
            val response = apiServices.getListEmployees(pageIndex = nextPage, fullName = fullName)
            var nextPageNumber: Int? = null
            if (response.content.hasNextPage) {
                nextPageNumber = nextPage + 1
            }

            LoadResult.Page(
                data = response.content.items,
                prevKey = null,
                nextKey = nextPageNumber
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 1
    }
}